Quando("eu selecionar os itens e os colocar no carrinho") do
    @carrega_carrinho = Remover_item.new
    @acoes = Solicitacoes.new
    qtde_itens=3
    begin
        @acoes.coloca_no_carrinho
        qtde_itens-=1
    end while qtde_itens > 0
    @acoes.abre_carrinho
    @acoes.conclui_compra
  end
  
  Quando("efetuo a compra") do
    @paga = Pagamento.new
    @paga.insere_dados_da_compra 'São Paulo', 'rafaelo@email.com.br', 'Rafa', 'O', 'Testes', 'São Paulo', 'São Paulo', '11900009999'
    @paga.confirma_frete
    @acoes.fecha_compra
  end
  
  Então("valido compra") do
    @acoes.valida_qtde_carrinho
    puts "Compra realizada"
  end