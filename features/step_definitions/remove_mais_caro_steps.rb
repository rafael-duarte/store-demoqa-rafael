Dado("que tenha acesso ao site") do
    @compra = Visitar.new.load
end
  
Quando("eu selecionar {int} itens e os colocar no carrinho") do |qtde_itens|
    @carrega_carrinho = Remover_item.new
    @acoes = Solicitacoes.new
    begin
        @acoes.coloca_no_carrinho
        qtde_itens-=1
    end while qtde_itens > 0
    @acoes.abre_carrinho
end
  
Quando("remover o mais caro dos itens") do
    @carrega_carrinho.retira_mais_caro
end
  
Então("eu efetuo a compra") do
    @acoes.valida_qtde_carrinho
end
  