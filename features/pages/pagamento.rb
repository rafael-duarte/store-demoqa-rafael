class Pagamento < SitePrism::Page

    element :select_pais0_element, '#uniform-current_country'
    element :email_element ,'#wpsc_checkout_form_9'
    element :name_element ,'#wpsc_checkout_form_2'
    element :last_name_element, '#wpsc_checkout_form_3'
    element :endereco_element, '#wpsc_checkout_form_4'
    element :cidade_element, '#wpsc_checkout_form_5'
    element :estado_element, '#wpsc_checkout_form_6'
    element :select_pais1_element, '#uniform-wpsc_checkout_form_7'
    element :postal_code_element, '#wpsc_checkout_form_8'
    element :fone_element, '#wpsc_checkout_form_18'
    element :frete, '#shippingSameBilling'
    element :localidade_element, :xpath, '//*[@id="change_country"]/input[2]'
    element :btn_frete, :xpath, '//*[@id="change_country"]/input[4]'
    
    def insere_dados_da_compra (estado1, email1, nome, last_name, endereco1, city, estado2, telefone)
        select_pais0_element.select('Brazil')
        localidade_element.set(estado1)
        wait_for_frete
        btn_frete.click
        email_element.set(email1)
        name_element.set(nome)
        last_name_element.set(last_name)
        endereco_element.set(endereco1)
        cidade_element.set(city)
        estado_element.set(estado2)
        postal_code_element.set('cdsa')
        fone_element.set(telefone)
        select_pais1_element.select('Brazil')
        frete.text
    end
    
    def confirma_frete
        valorDoFrete = frete.text.sub! '$',''
        puts "O valor do frete é #{valorDoFrete}"
        valor_f = valorDoFrete.to_f
        if valor_f >0
            puts "O valor do frete é válido"
        else
            puts "O valor do frete não é válido"
        end
    end

end