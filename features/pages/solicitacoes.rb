class Solicitacoes < SitePrism::Page

    elements :qtde_itens, '.product_image'
    element :carrinho, '.cart_icon'
    element :item, :xpath, '//*[@id="footer"]/section[2]/ul/li[1]/a[2]/img'
    element :conclui_compra_btn, '.step2'
    element :item_enviado_ao_carrinho, '.alert addtocart'
    element :fecha_compra_btn, '.input-button-buy'

    def coloca_no_carrinho
        wait_for_item
        item.click
        click_button('Add To Cart') 
        wait_for_item_enviado_ao_carrinho
    end

    def abre_carrinho
        carrinho.click
        wait_for_qtde_itens
    end

    def conclui_compra
        wait_for_conclui_compra_btn
        conclui_compra_btn.click
    end

    def valida_qtde_carrinho
        puts "A nova quantidade é #{qtde_itens.size}"
    end

    def fecha_compra
        fecha_compra_btn.click
    end

end