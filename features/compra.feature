#language:pt
#utf-8

Funcionalidade: Comprar no site demoqa
    Eu como cliente do site demoqa
    Quero colocar itens no carrinho
    Para efetuar uma compra

    Contexto: Acesso ao site
        Dado que tenha acesso ao site

    @remove
    Esquema do Cenario: Comprar e remover o mais caro
        Quando eu selecionar <qtde> itens e os colocar no carrinho
        E remover o mais caro dos itens
        Então eu efetuo a compra

        Exemplos:

        |   qtde    |
        |    3      |

    @compra3
    Cenario: Comprar
        Quando eu selecionar os itens e os colocar no carrinho
        E efetuo a compra
        Então valido compra

