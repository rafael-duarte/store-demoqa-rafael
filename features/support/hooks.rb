After do |scenario|
    page.driver.browser.save_screenshot("#{scenario.__id__}.png")
    Capybara.current_session.driver.browser.manage.delete_all_cookies
    Capybara.current_session.driver.quit
    Capybara.page.driver.browser.manage.window.maximize
end